package minesweeper.text;

import minesweeper.View;
import minesweeper.controller.Controller;
import minesweeper.model.Model;

import java.util.ArrayList;
import java.util.Scanner;

public class Text implements View {
    private Controller controller;
    private boolean win;
    private boolean fail;
    public Text(){
        win = false;
        fail = false;
    }
    public void go() {
        String command;
        Scanner sc = new Scanner(System.in);
        controller.setMode(askmode());
        Integer x, y;
        while (!fail && !win){
            command  = sc.next();
            switch (command){
                case "New":
                    sc.next();
                    controller.setMode(askmode());
                    break;
                case "Exit":
                    return;
                case "About":
                    System.out.print("It's the minesweeper\n\nFirstly, you sed mode.\n" +
                            "Then, you send coordinates in which you want to go.\n" +
                            "You have some commands:\n" +
                            "New Game:  Game started again.\nExit:  End game.\n" +
                            "About:  Find out about game.\nHigh score:  Print scores.\n" +
                            "Shoot coordinate1 coordinate2: shoot.\n" +
                            "Mark coordinate1 coordinate2: mark\n");
                    break;
                case "High":
                    //AAAAAAAAAAAAAAAAAAAAAAAAAA
                    break;
                case "Shoot":
                    x = Integer.parseInt(sc.next());
                    y = Integer.parseInt(sc.next());
                    controller.shoot(x, y);
                    if (fail || win)
                        return;
                    break;
                case "Mark":
                    x = Integer.parseInt(sc.next());
                    y = Integer.parseInt(sc.next());
                    controller.mark(x, y);
                default:
                    break;
            }
        }
    }
    public boolean isover(){
        return !win || !fail;
    }

    public Parameters askmode() {
        System.out.println("Enter difficulty:\n EASY, MEDIUM, HARD, OWN");
        Scanner sc = new Scanner(System.in);
        String answer = sc.next();
        Parameters parameters;
        switch (answer) {
            case "EASY":
                parameters = new Parameters(9, 9, 10);
                break;
            case "MEDIUM":
                parameters = new Parameters(16, 16, 40);
                break;
            case "HARD":
                parameters = new Parameters(16, 30, 99);
                break;
            case "OWN":
                System.out.println("Enter length, width and mines trough a space:");
                Integer length = Integer.getInteger(sc.next());
                Integer width = Integer.getInteger(sc.next());
                Integer mines = Integer.getInteger(sc.next());
                parameters = new Parameters(length, width, mines);
                break;
            default:
                System.out.println("Entered data was not recognised. Easy mode was set");
                parameters = new Parameters(9, 9, 10);
                break;
        }
        return parameters;
    }
    public void setTimer(String timer){

    }

    public void setController(Controller controller){
        this.controller = controller;
    }

    public void setWin(){
        win = true;
    }
    public void setFail() {fail = true;}
    public void printfield(ArrayList<Model.Condition> list, Integer width, Integer length){
        Model.Condition cond;
        for (int i = 0; i < length; ++i){
            for (int j = 0; j < width; ++j){
                cond = list.get(i*width + j);
                if (!cond.opened){
                    if(cond.mine && fail)
                        System.out.print("+");
                    else if (cond.marked)
                        System.out.print("*");
                    else
                        System.out.print(".");
                } else {
                    System.out.print(cond.minesAround);
                }
            }
            System.out.print("\n");
        }
        if(fail)
            System.out.print("FAIL\n");
        if(win)
            System.out.print("WIN\n");
    }
}
