package minesweeper.text;

public class Parameters {
    private Integer length;
    private Integer width;
    private Integer mines;

    public Parameters(Integer length, Integer width, Integer mines) {
        this.length = length;
        this.mines = mines;
        this.width = width;
    }

    public Integer getWidth() {
        return width;
    }

    public Integer getMines() {
        return mines;
    }

    public Integer getLength() {
        return length;
    }
}
