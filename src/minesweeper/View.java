package minesweeper;

import minesweeper.controller.Controller;
import minesweeper.model.Model;
import minesweeper.text.Parameters;

import java.util.ArrayList;

public interface View {
    void go();
    Parameters askmode();
    void setController(Controller controller);
    void setWin();
    void setFail();
    void printfield(ArrayList<Model.Condition> list, Integer width, Integer length);
    boolean isover();
    void setTimer(String timer);
}
