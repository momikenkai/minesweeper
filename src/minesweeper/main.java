package minesweeper;

import minesweeper.controller.Controller;
import minesweeper.gui.Gui;
import minesweeper.model.Model;
import minesweeper.text.Text;

public class main {
    public static void main(String[] args){
        if (args.length != 1){
            System.out.println("Bad args.\n\nPlease enter one of two keys '-g' or '-t'.\n");
            return;
        }
        if(args[0].equals("-g")){
            Model model = new Model();
            View gui = new Gui();
            Controller controller = new Controller();
            model.setView(gui);
            model.startTimer();
            gui.setController(controller);
            controller.setModel(model);
            gui.go();
        }else if(args[0].equals("-t")){
            Model model = new Model();
            View tx = new Text();
            Controller controller = new Controller();
            model.setView(tx);
            tx.setController(controller);
            controller.setModel(model);
            tx.go();
        } else{
            System.out.println("Bad args.\n\nPlease enter one of two keys '-g' or '-t'.\n");
            return;
        }
    }
}
