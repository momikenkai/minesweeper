package minesweeper.controller;

import minesweeper.model.Model;
import minesweeper.text.Parameters;

import java.util.concurrent.atomic.AtomicInteger;

public class Controller {
    private Model model;
    public Controller(){}

    public void setModel(Model model) {
        this.model = model;
    }
    public void setMode(Parameters parameters){
        model.setParameters(parameters);
    }
    public void shoot(int x, int y){
        model.shoot(x, y);
    }
    public void mark(int x, int y){
        model.mark(x,y);
    }
    public void startTime(){
        model.timenull();
    }
}
