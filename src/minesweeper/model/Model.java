package minesweeper.model;
import minesweeper.View;
import minesweeper.gui.Gui;
import minesweeper.text.Parameters;
import minesweeper.text.Text;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class Model implements Runnable {
    private AtomicInteger time;
    private class Pair{
        Integer x;
        Integer y;
        public Pair(Integer x, Integer y){
            this.x = x;
            this.y = y;
        }
        public Integer getX() {
            return x;
        }
        public Integer getY() {
            return y;
        }
    }
    public class Condition{
        public boolean opened;
        public boolean mine;
        public boolean marked;
        public int minesAround;
        public Condition(){
            opened = false;
            mine = false;
            marked = false;
            minesAround = 0;
        }
    }
    private ArrayList<Condition> field;
    private Integer length;
    private Integer width;
    private Integer mines;
    private int opened;
    private View view;

    public  Model(){
        opened = 0;
        time = new AtomicInteger(0);
    }
    public void startTimer(){
        Thread myThready = new Thread(this);
        myThready.start();
    }
    public void setParameters(Parameters parameters){
        length = parameters.getLength();
        width = parameters.getWidth();
        mines = parameters.getMines();
        opened = 0;
        field = new ArrayList<>(length * width);
        for(int i = 0; i < length * width; ++i){
            Condition cond = new Condition();
            field.add(i, cond);
        }
        for(int i = 0; i < parameters.getMines(); ++i){
            Random random = new Random();
            Condition cond;
            do {
                int n = random.nextInt(length*width - 1);
                cond = field.get(n);
            }while (cond.mine);
            cond.mine = true;
        }
        for(int i = 0; i < length; ++i){
            for(int j = 0; j < width; ++j){
                int mine = 0;
                Condition cond = field.get(i*width + j);
                if (i != 0) {
                    if (field.get((i - 1) * width + j).mine)
                        mine++;
                    if (j != 0)
                        if(field.get((i - 1)*width + j - 1).mine)
                            mine++;
                    if (j != width - 1)
                        if(field.get((i - 1)*width + j + 1).mine)
                            mine++;
                }
                if(i != length - 1) {
                    if (field.get((i + 1) * width + j).mine)
                        mine++;
                    if (j != 0)
                        if(field.get((i + 1)*width + j - 1).mine)
                            mine++;
                    if (j != width - 1)
                        if(field.get((i + 1)*width + j + 1).mine)
                            mine++;
                }
                if (j != 0)
                    if(field.get(i*width + j - 1).mine)
                        mine++;
                if(j != width - 1)
                    if(field.get(i*width + j + 1).mine)
                        mine++;
                cond.minesAround = mine;
            }
        }
        view.printfield(field, width, length);
    }
    public int shoot(int x, int y){
        Condition cond = field.get(x*width + y);
        if(cond.marked){
            return 0;
        }
        if (cond.mine) {
            view.setFail();
            view.printfield(field, width, length);
            return 0;
        }
        if(cond.opened) {
            view.printfield(field, width, length);
            return 0;
        }
        if(cond.minesAround != 0) {
            cond.opened = true;
            opened++;
            if(opened == width*length - mines)
                view.setWin();
            view.printfield(field, width, length);
            return 0;
        } else {
            Queue<Pair> queue = new LinkedList<>();
            HashMap<Integer, Boolean> map = new HashMap<>();
            queue.add(new Pair(x, y));
            map.put(x * width + y, true);
            Pair pair;
            while (!queue.isEmpty()){
                pair = queue.remove();
                cond = field.get(pair.x*width + pair.y);
                open(cond);
                if (pair.x > 0){
                    cond = field.get((pair.x - 1)*width + pair.y);
                    if(cond.minesAround == 0 && !cond.opened && map.get((pair.x - 1)*width + pair.y) == null) {
                        queue.add(new Pair(pair.x - 1, pair.y));
                        map.put((pair.x - 1)*width + pair.y, true);
                    }
                    else {
                        open(cond);
                    }
                    if (pair.y > 0) {
                        cond = field.get((pair.x - 1) * width + pair.y - 1);
                        if (cond.minesAround == 0 && !cond.opened && map.get((pair.x - 1)*width + pair.y - 1) == null) {
                            queue.add(new Pair(pair.x - 1, pair.y - 1));
                            map.put((pair.x - 1)*width + pair.y - 1, true);
                        }
                        else {
                            open(cond);
                        }
                    }
                    if (pair.y < width - 1) {
                        cond = field.get((pair.x - 1) * width + pair.y + 1);
                        if (cond.minesAround == 0 && !cond.opened && map.get((pair.x - 1)*width + pair.y + 1) == null) {
                            queue.add(new Pair(pair.x - 1, pair.y + 1));
                            map.put((pair.x - 1)*width + pair.y + 1, true);
                        }
                        else {
                            open(cond);
                        }
                    }
                }
                if(pair.x < length - 1){
                    cond = field.get((pair.x + 1)*width + pair.y);
                    if(cond.minesAround == 0 && !cond.opened && map.get((pair.x + 1)*width + pair.y) == null) {
                        queue.add(new Pair(pair.x + 1, pair.y));
                        map.put((pair.x + 1)*width + pair.y, true);
                    }
                    else {
                        open(cond);
                    }
                    if (pair.y > 0) {
                        cond = field.get((pair.x + 1) * width + pair.y - 1);
                        if (cond.minesAround == 0 && !cond.opened && map.get((pair.x + 1)*width + pair.y - 1) == null) {
                            queue.add(new Pair(pair.x + 1, pair.y - 1));
                            map.put((pair.x + 1)*width + pair.y - 1, true);
                        }
                        else {
                            open(cond);
                        }
                    }
                    if (pair.y < width - 1) {
                        cond = field.get((pair.x + 1) * width + pair.y + 1);
                        if (cond.minesAround == 0 && !cond.opened && map.get((pair.x + 1)*width + pair.y + 1) == null) {
                            queue.add(new Pair(pair.x + 1, pair.y + 1));
                            map.put((pair.x + 1)*width + pair.y + 1, true);
                        }
                        else {
                            open(cond);
                        }
                    }
                }
                if (pair.y > 0) {
                    cond = field.get(pair.x * width + pair.y - 1);
                    if (cond.minesAround == 0 && !cond.opened && map.get(pair.x*width + pair.y - 1) == null) {
                        queue.add(new Pair(pair.x, pair.y - 1));
                        map.put(pair.x*width + pair.y - 1, true);
                    }
                    else {
                        open(cond);
                    }
                }
                if (pair.y < width - 1) {
                    cond = field.get(pair.x * width + pair.y + 1);
                    if (cond.minesAround == 0 && !cond.opened && map.get(pair.x*width + pair.y + 1) == null) {
                        queue.add(new Pair(pair.x, pair.y + 1));
                        map.put(pair.x*width + pair.y + 1, true);
                    }
                    else {
                        open(cond);
                    }
                }
            }
        }
        view.printfield(field, width, length);
        return 0;
    }
    public void setView(View view){
        this.view = view;
    }
    public void mark(int x, int y){
        Condition cond = field.get(x*width + y);
        cond.marked = !cond.marked;
        view.printfield(field, width, length);
    }
    public void open(Condition cond){
        if(!cond.opened) {
            cond.opened = true;
            opened++;
            if (opened == width * length - mines)
                view.setWin();
        }
    }

    public void run() {
        while (true) {
            time.incrementAndGet();
            if(!view.isover()){
                String tim;
                int min = time.intValue() / 60;
                int sec = time.intValue() % 60;
                String secc = sec < 10 ? "0" + sec : String.valueOf(sec);
                tim = min + ":" + secc;
                view.setTimer(tim);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        }
    }
    public void timenull(){
        time = new AtomicInteger(0);
    }

    public AtomicInteger getTime() {
        return time;
    }
}
