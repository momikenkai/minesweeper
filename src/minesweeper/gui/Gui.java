package minesweeper.gui;

import minesweeper.View;
import minesweeper.controller.Controller;
import minesweeper.model.Model;
import minesweeper.text.Parameters;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class Gui implements View {
    private Controller controller;
    private JFrame frame;
    private JPanel difficultyPanel;
    private JPanel buttonsPanel;
    private JPanel highScoresPanel;
    private ArrayList<Button> buttons;
    private Integer widthButtons;
    private Integer lengthButtons;
    private boolean win;
    private boolean fail;
    private JLabel timerLabel;

    public Gui() {
        frame = new JFrame("Minesweeper");
        frame.setResizable(false);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setBounds(new Rectangle(100, 100, 600, 500));
        difficultyPanel = new JPanel();
        buttonsPanel = new JPanel();
        highScoresPanel = new JPanel();
        frame.setLayout(new BorderLayout());
        timerLabel = new JLabel("00:00");
        win = false;
        fail = false;
        makedifficultypanel();
    }

    public void go() {

//        ImageIcon icon = new ImageIcon("icon.png");
//        frame.setIconImage(icon.getImage());
        Image image = Toolkit.getDefaultToolkit().createImage(getClass().getResource("icon.png"));
        frame.setIconImage(image);

        JMenuBar menuBar = new JMenuBar();
        JMenu menu = new JMenu("Game");
        JMenuItem newGame = new JMenuItem("New Game");
        menu.add(newGame);
        JMenuItem exit = new JMenuItem("Exit");
        menu.add(exit);
        JMenuItem about = new JMenuItem("About");
        menu.add(about);
        JMenuItem highScores = new JMenuItem("High Scores");
        menu.add(highScores);;
        menuBar.add(menu);
        frame.setJMenuBar(menuBar);
        frame.add(difficultyPanel);
        frame.setVisible(true);

        newGame.addActionListener(e -> askmode());
        exit.addActionListener(e -> frame.dispose());
        about.addActionListener(e -> {
            JDialog dialog = new JDialog(frame);
            dialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
            dialog.setBounds(new Rectangle(300, 300, 220, 100));
            dialog.setTitle("About");
            JLabel label = new JLabel("<html><div style='text-align: center;'>Minesweeper. All rights reserved (c)</div></html>");
            dialog.setLayout(new BorderLayout());
            dialog.add(label, BorderLayout.CENTER);
            dialog.setResizable(false);

            dialog.setVisible(true);
        });
        highScores.addActionListener(e -> printScores());

    }

    public Parameters askmode() {

        buttons = null;
        buttonsPanel.removeAll();
        frame.setBounds(new Rectangle(100, 100, 600, 500));
        frame.getContentPane().removeAll();
        frame.add(difficultyPanel, BorderLayout.CENTER);
        return null;
    }

    public void setController(Controller controller) {
        this.controller = controller;
    }

    public void setWin() {
        win = true;
    }

    public void setFail() {
        fail = true;
    }

    public void printfield(ArrayList<Model.Condition> list, Integer width, Integer length) {
        if (!win || !fail) {
            if (buttons == null) {
                buttons = new ArrayList<>(length * width);
                for (int i = 0; i < length; i++) {
                    for (int j = 0; j < width; j++) {
                        Button button = new Button(i, j);
                        button.setcondition(list.get(i * width + j));
                        button.addMouseListener(new MouseListener() {
                            @Override
                            public void mouseClicked(MouseEvent e) {
                                if (SwingUtilities.isRightMouseButton(e)) {
                                    controller.mark(button.getXX(), button.getYY());
                                } else {
                                    controller.shoot(button.getXX(), button.getYY());
                                }
                            }
                            @Override
                            public void mousePressed(MouseEvent e) { }
                            @Override
                            public void mouseReleased(MouseEvent e) { }
                            @Override
                            public void mouseEntered(MouseEvent e) { }
                            @Override
                            public void mouseExited(MouseEvent e) { }
                        });
                        buttonsPanel.add(button);
                        buttons.add(button);
                    }
                }
                frame.setVisible(true);
            } else {
                for (int i = 0; i < length; i++) {
                    for (int j = 0; j < width; j++) {
                        buttons.get(i * width + j).setcondition(list.get(i * width + j));
                    }
                }
            }
        }
        if (fail) {
            JDialog dialog = new JDialog(frame);
            dialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
            dialog.setBounds(new Rectangle(100, 100, 100, 100));
            dialog.setTitle("LOOOSEEER!!");
            JLabel label = new JLabel("<html><div style='text-align: center;'>    Sorry, you lost. Try again </div></html>");
            dialog.setLayout(new BorderLayout());
            dialog.add(label, BorderLayout.CENTER);
            dialog.setLocationRelativeTo(frame);
            dialog.setResizable(false);

            dialog.setVisible(true);
        }
        if(win){
            JDialog dialog = new JDialog(frame);
            dialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
            dialog.setBounds(new Rectangle(100, 100, 220, 200));
            dialog.setTitle("WINEEEEER!!");
            JLabel label = new JLabel("<html><div style='text-align: center;'> Enter your name </div></html>");
            JTextField name = new JTextField(5);
            JPanel panel = new JPanel();
            panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
            panel.add(label);
            name.setBounds(new Rectangle(10, 5));
            panel.add(name);
            JButton button = new JButton("set");
            button.setBounds(new Rectangle(5, 5));
            button.addActionListener(e -> newscore(name.getText()));
            panel.add(button);
            dialog.add(panel);
            dialog.setLocationRelativeTo(frame);
            dialog.setResizable(false);
            dialog.setVisible(true);
        }
    }

    private void makedifficultypanel() {
        JLabel label = new JLabel("Выберите сложность");
        Font font = new Font("Verdana", Font.ITALIC, 20);
        label.setFont(font);
        JButton buttonEasy = new JButton("EASY");
        JButton buttonMedium = new JButton("MEDIUM");
        JButton buttonHard = new JButton("HARD");
        JButton buttonOwn = new JButton("OWN");
        buttonEasy.addActionListener(e -> {
            widthButtons = 9;
            lengthButtons = 9;
            fail = false;
            win = false;
            Parameters parameters = new Parameters(9, 9, 10);
            buttonsPanel.setLayout(new GridLayout(9, 9, 0, 0));
            frame.setBounds(new Rectangle(widthButtons * 32, lengthButtons * 32 + 70));
            controller.setMode(parameters);
            //.getContentPane().removeAll();
            frame.remove(difficultyPanel);
            timerLabel.setText("00:00");
            frame.add(timerLabel, BorderLayout.NORTH);
            frame.add(buttonsPanel, BorderLayout.CENTER);
            controller.startTime();
            frame.setVisible(true);
        });
        buttonMedium.addActionListener(e -> {
            widthButtons = 16;
            lengthButtons = 16;
            fail = false;
            win = false;
            Parameters parameters = new Parameters(16, 16, 40);
            buttonsPanel.setLayout(new GridLayout(16, 16, 0, 0));
            frame.setBounds(new Rectangle(widthButtons * 32, lengthButtons * 32 + 70));
            controller.setMode(parameters);
            //frame.getContentPane().removeAll();
            frame.remove(difficultyPanel);
            timerLabel.setText("00:00");
            frame.add(timerLabel, BorderLayout.NORTH);
            frame.add(buttonsPanel, BorderLayout.CENTER);
            controller.startTime();
            frame.setVisible(true);
        });
        buttonHard.addActionListener(e -> {
            widthButtons = 30;
            lengthButtons = 16;
            fail = false;
            win = false;
            Parameters parameters = new Parameters(16, 30, 99);
            buttonsPanel.setLayout(new GridLayout(16, 30, 0, 0));
            frame.setBounds(new Rectangle(widthButtons * 32, lengthButtons * 32 + 70));
            controller.setMode(parameters);
            //frame.getContentPane().removeAll();
            frame.remove(difficultyPanel);
            timerLabel.setText("00:00");
            frame.add(timerLabel, BorderLayout.NORTH);
            frame.add(buttonsPanel, BorderLayout.CENTER);
            controller.startTime();

            frame.setVisible(true);
        });
        buttonOwn.addActionListener(e -> {
            JPanel panel = new JPanel();
            JPanel panelLength = new JPanel();
            JPanel panelWidth = new JPanel();
            JPanel panelMines = new JPanel();
            JTextField lengthField = new JTextField(5);
            JTextField widthField = new JTextField(5);
            JTextField minesField = new JTextField(5);
            JLabel labelLength = new JLabel("Длина:");
            JLabel labelWidth = new JLabel("Ширина:");
            JLabel labelMines = new JLabel("Мины:");
            panelLength.add(labelLength);
            panelLength.add(lengthField);
            panelWidth.add(labelWidth);
            panelWidth.add(widthField);
            panelMines.add(labelMines);
            panelMines.add(minesField);
            panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
            panel.add(panelLength);
            panel.add(panelWidth);
            panel.add(panelMines);
            JButton startButton = new JButton("Начать");
            startButton.addActionListener(e1 -> {
                fail = false;
                win = false;
                Parameters parameters = new Parameters(Integer.parseInt(lengthField.getText()),
                        Integer.parseInt(widthField.getText()),
                        Integer.parseInt(minesField.getText()));
                buttonsPanel.setLayout(new GridLayout(Integer.parseInt(lengthField.getText()),
                        Integer.parseInt(widthField.getText()), 0, 0));
                controller.setMode(parameters);
                frame.getContentPane().removeAll();
                timerLabel.setText("00:00");
                frame.add(timerLabel, BorderLayout.NORTH);
                frame.add(buttonsPanel, BorderLayout.CENTER);
                controller.startTime();
                frame.setBounds(new Rectangle(Integer.parseInt(widthField.getText()) * 32, Integer.parseInt(lengthField.getText()) * 32 + 70));
            });
            panel.add(startButton);
            //frame.getContentPane().removeAll();
            frame.remove(difficultyPanel);
            frame.add(panel, BorderLayout.CENTER);
            frame.setVisible(true);

        });
        difficultyPanel.add(label);
        difficultyPanel.add(buttonEasy);
        difficultyPanel.add(buttonMedium);
        difficultyPanel.add(buttonHard);
        difficultyPanel.add(buttonOwn);
    }

    private void printScores() {
        HashMap<String, Integer> map = new HashMap<>();
        InputStream in = getClass().getResourceAsStream("config.txt");
        Scanner sc = new Scanner(in);
    }

    private void newscore(String name){

    }

    public boolean isover() {
        return win || fail;
    }

    public void setTimer(String timer) {
        timerLabel.setText(timer);
    }

    public class Button extends JButton {
        private Integer x;
        private Integer y;

        public Button(Integer x, Integer y) {
            super();
            this.x = x;
            this.y = y;
            // setPreferredSize(new Dimension(32,32));
            setHorizontalTextPosition(JButton.CENTER);
            setVerticalTextPosition(JButton.CENTER);
            setMargin(new Insets(0, 0, 0, 0));
            try {
                setIcon(new ImageIcon(getClass().getResource("close.jpg").toURI().toURL()));
            } catch (MalformedURLException | URISyntaxException e) {
                e.printStackTrace();
            }
            /*
            }*/
        }

        public void setcondition(Model.Condition condition) {
            if (!condition.opened && !fail) {
                if (!condition.marked) {
                    try {
                        setIcon(new ImageIcon(getClass().getResource("close.jpg").toURI().toURL()));
                    } catch (MalformedURLException | URISyntaxException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        setIcon(new ImageIcon(getClass().getResource("flag.jpg").toURI().toURL()));
                    } catch (MalformedURLException | URISyntaxException e) {
                        e.printStackTrace();
                    }
                }
            } else if (fail && condition.mine) {
                try {
                    setIcon(new ImageIcon(getClass().getResource("mine.jpg").toURI().toURL()));
                } catch (MalformedURLException | URISyntaxException e) {
                    e.printStackTrace();
                }
            } else {
                switch (condition.minesAround) {
                    default:
                    case 0:
                        try {
                            setIcon(new ImageIcon(getClass().getResource("open.jpg").toURI().toURL()));
                        } catch (MalformedURLException | URISyntaxException e) {
                            e.printStackTrace();
                        }
                        break;
                    case 1:
                        try {
                            setIcon(new ImageIcon(getClass().getResource("1.jpg").toURI().toURL()));
                        } catch (MalformedURLException | URISyntaxException e) {
                            e.printStackTrace();
                        }
                        break;
                    case 2:
                        try {
                            setIcon(new ImageIcon(getClass().getResource("2.jpg").toURI().toURL()));
                        } catch (MalformedURLException | URISyntaxException e) {
                            e.printStackTrace();
                        }
                        break;
                    case 3:
                        try {
                            setIcon(new ImageIcon(getClass().getResource("3.jpg").toURI().toURL()));
                        } catch (MalformedURLException | URISyntaxException e) {
                            e.printStackTrace();
                        }
                        break;
                    case 4:
                        try {
                            setIcon(new ImageIcon(getClass().getResource("4.jpg").toURI().toURL()));
                        } catch (MalformedURLException | URISyntaxException e) {
                            e.printStackTrace();
                        }
                        break;
                    case 5:
                        try {
                            setIcon(new ImageIcon(getClass().getResource("5.jpg").toURI().toURL()));
                        } catch (MalformedURLException | URISyntaxException e) {
                            e.printStackTrace();
                        }
                        break;
                    case 6:
                        try {
                            setIcon(new ImageIcon(getClass().getResource("6.jpg").toURI().toURL()));
                        } catch (MalformedURLException | URISyntaxException e) {
                            e.printStackTrace();
                        }
                        break;
                    case 7:
                        try {
                            setIcon(new ImageIcon(getClass().getResource("7.jpg").toURI().toURL()));
                        } catch (MalformedURLException | URISyntaxException e) {
                            e.printStackTrace();
                        }
                        break;
                    case 8:
                        try {
                            setIcon(new ImageIcon(getClass().getResource("8.jpg").toURI().toURL()));
                        } catch (MalformedURLException | URISyntaxException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        }

        public Integer getXX() {
            return x;
        }

        public Integer getYY() {
            return y;
        }
    }
}



